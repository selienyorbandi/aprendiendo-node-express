/* const http = require("http");  // Sin express */
const express = require("express");
const app = express();
const logger = require("./loggerMiddleware");

app.use(express.json());
app.use(logger);

let trendingItems = [
  {
    name: "Placa de Video Zotac GeForce RTX 3080 10GB GDDR6X Trinity LHR",
    id: 1,
    description:
      "Obtenga amplificación con la serie ZOTAC GAMING GeForce RTX 30 basada en la arquitectura NVIDIA Ampere. Construida con RT Cores y Tensor Cores mejorados, nuevos multiprocesadores de transmisión y memoria GDDR6X ultrarrápida, la ZOTAC GAMING GeForce RTX 3080 Trinity OC da lugar a juegos amplificados con ultra fidelidad de gráficos.",
    price: 290000,
    img: "https://i.postimg.cc/tTQkD7jb/Placa-de-Video-Zotac-Ge-Force-RTX-3080-10-GB-GDDR6-X-Trinity-LHR.jpg",
    stock: 6,
  },
  {
    name: "Notebook Dell G5 AMD Ryzen 5 5600H 8GB SSD 512GB Nvidia 15.6 W10",
    id: 2,
    description:
      "Nada detendrá tu productividad gracias a su enfriamiento de ventilador doble y tecnología SmartShift, que distribuye automáticamente la energía entre el CPU y GPU, para mejorar el rendimiento en juegos, edición de video, renderizado 3D, productividad y creación de contenido, aún en los momentos más intensos.Wifi de última generación \n Evita perder una partida por retrasos y desconexiones gracias a su conectividad Killer Wireless WiFi 6 AX1650 (2x2), que te garantiza una velocidad increíblemente rápida para un tráfico de red sin inconvenientes.\n Conectividad a tope \n Conecta tus controles, audífonos, teclados o ratones gaming, cuenta con 2 USB 2.0, 1 USB tipo-A 3.0 (3.1 Gen 1), además de 1 puerto tipo-C USB 3.1 (3.1 Gen 2), 1 HDMI 2.0, 1 combo de salida de auriculares/micrófono, 1 puerto Mini Display y tecnología inalámbrica Bluetooth 5.1.",
    price: 180000,
    img: "https://i.postimg.cc/tgVDhW8D/notebook-dell-156-inspiron-g5-ryzen-5-8gb-512gb-win10-1.jpg",
    stock: 8,
  },
  {
    name: "Unidad de estado sólido Falcon PCIe Gen3x4 M.2 2280",
    id: 3,
    description:
      "Aumente su poder creativo con la unidad de estado sólido (SSD) ADATA FALCON. Utilizando la interfaz PCIe Gen3x4 y equipada con una memoria flash 3D NAND, la unidad FALCON ofrece una velocidad de lectura/escritura de hasta 31 000/1500 MB por segundo para una productividad y creatividad ininterrumpidas.",
    price: 10499,
    img: "https://i.postimg.cc/tJyj0XhQ/adata-falcon-512-ssd.jpg",
    stock: 21,
  },
  {
    name: "Procesador AMD Ryzen 7 5700G 4.6GHz Turbo + Wraith Stealth Cooler",
    id: 4,
    description:
      "Clave en el rendimiento de tu computadora de escritorio, ya no tenés que pensar en cómo distribuir el tiempo y acciones porque ahora las tareas en simultáneo son posibles. \n AMD cuenta con un catálogo de productos que se adaptan a los requerimientos de todo tipo de usuarios: juegos en línea, edición a gran escala, contenido en múltiples plataformas y más. \n Núcleos: el corazón del procesador \n En este producto, encontrarás los núcleos, que son los encargados de ejecutar las instrucciones y actividades que le asignás a tu dispositivo. Estos tienen relación directa con dos elementos: los hilos y el modelo. Por lo tanto, a la hora de elegir un procesador, es importante que valores los tres en su conjunto.\n Máxima potencia\n Al estar desbloqueado, podrás realizar overclocking y así aumentar la frecuencia de funcionamiento y optimizar el rendimiento de tu equipo. Personalizalo a tu gusto y disfrutá de tus videojuegos o hacé que la renderización de imágenes sea más ágil. ¡Descubrí el abanico de posibilidades que esta función te ofrece!",
    price: 54200,
    img: "https://i.postimg.cc/85zYLjBC/Procesador-AMD-Ryzen-7-5700-G-4-6-GHz-Turbo-Wraith-Stealth-Cooler.jpg",
    stock: 13,
  },
  {
    name: "Placa de Video MSI Radeon RX 6600 8GB GDDR6 MECH 2X",
    id: 5,
    description:
      "AMD es un fabricante estadounidense de placas de video, por su tecnología se ha destacado en crear procesadores de alta gama que permiten un excelente funcionamiento del motor gráfico de tu computadora. \n Velocidad en cada lectura \n Como cuenta con 2048 núcleos, los cálculos para el procesamiento de gráficos se realizarán de forma simultánea logrando un resultado óptimo del trabajo de la placa. Esto le permitirá ejecutar lecturas dispersas y rápidas de y hacia la GPU.\n Calidad de imagen \n Criterio fundamental a la hora de elegir una placa de video, su resolución de 7680x4320 no te defraudará. La decodificación de los píxeles en tu pantalla te harán ver hasta los detalles más ínfimos en cada ilustración.",
    price: 115000,
    img: "https://i.postimg.cc/HW42TpQZ/Placa-de-Video-MSI-Radeon-RX-6600-8-GB-GDDR6-MECH-2x.jpg",
    stock: 7,
  },
  {
    name: "Auriculares HyperX Cloud Alpha Red",
    id: 6,
    description:
      "El innovador diseño del Cloud Alpha de HyperX™ con tecnología de altavoces con cámara doble te brindará un sonido con mayor nivel de nitidez y claridad gracias a la reducción de distorsión. La cámara doble separa los graves de los medios y agudos, permitiendo una sintonización óptima que producirá un sonido más limpio y suave. Los audífonos Cloud Alpha ostentan la fama de la galardonada comodidad de HyperX, con memory foam premium rojo, banda de sujeción expandida y cuero suave más flexible. La estructura de aluminio está construida para una mayor durabilidad, y pensando en las necesidades de cada jugador, el Cloud Alpha también cuenta con un resistente cable trenzado extraíble. El micrófono desmontable con cancelación de ruido está certificado por Discord y TeamSpeak™, lo cual te asegura que tendrás una gran calidad de comunicación con tu equipo. Compatibilidad multi-plataforma, con controles de audio en el cable, por eso los jugadores exigentes, ya sea de PC, PS4™, Xbox One™ y otras plataformas con puertos de 3.5mm se verán beneficiados con el sonido avanzado de la evolución del Cloud Alpha.",
    price: 10700,
    img: "https://i.postimg.cc/jdBQXLKj/hyperx-cloud-alpha-black-red.webp",
    stock: 9,
  },
  {
    name: "Mouse Logitech G-Pro Hero 16k",
    id: 7,
    description:
      "Perfecto para e-Sports \n PRO se inspira en el sencillo y clásico chasis de Logitech G100 y G100s, favorito de destacados profesionales de eSports. La uniformidad y el confort son fundamentales para lograr el máximo. El mouse PRO cumple en ambos casos.\n  Sistema mecánico de tensión \n El sistema mecánico de tensión de botones del mouse para juegos Logitech G PRO aumenta la coherencia de respuesta de los botones izquierdo y derecho del mouse, y requiere menos esfuerzo de click. Eso contribuye a la gran rapidez y confiabilidad de las acciones de click al más alto nivel de juego competitivo.",
    price: 3499,
    img: "https://i.postimg.cc/pT9fNFD4/logitech-g-pro-hero.jpg",
    stock: 14,
  },
  {
    name: "Monitor gamer Samsung F24T35 led 24\" azul y gris oscuro 100V/240V",
    id: 8,
    description:
      "Samsung está fielmente comprometida en brindar productos de calidad y que contribuyan a crear un mejor futuro para las personas. Como empresa líder en la industria tecnológica uno de sus objetivos principales es desarrollar avanzadas e innovadoras soluciones. Es por ello que con este monitor tendrás y disfrutarás de una gran experiencia visual en todo momento. \n Un monitor a tu medida \n Con tu pantalla LED no solo ahorrás energía, ya que su consumo es bajo, sino que vas a ver colores nítidos y definidos en tus películas o series favoritas. \n Una experiencia visual de calidad \n Este monitor de 24\" te va a resultar cómodo para estudiar, trabajar o ver una película en tus tiempos de ocio. Asimismo, su resolución de 1920 x 1080 te permite disfrutar de momentos únicos gracias a una imagen de alta fidelidad. Su tiempo de respuesta de 5 ms lo hace ideal para gamers y cinéfilos porque es capaz de mostrar imágenes en movimiento sin halos o bordes borrosos.",
    price: 28950,
    img: "https://i.postimg.cc/DZFTHrky/monitor-samsung-f24t35-led-24.webp",
    stock: 6,
  },
];

/* const app = http.createServer((request, response) => {
  response.writeHead(200, { "Content-Type": "application/json" });
  response.end(JSON.stringify(trendingItems));
}); // Sin express */

//? ____________GET_____________

app.get("/", (request, response) => {
  response.send("<h1>Hola mundo, este es el inicio</h1>");
});
app.get("/trendingItems", (request, response) => {
  response.json(trendingItems);
});

// Buscador por id
app.get("/trendingItems/:id", (request, response) => {
  const id = parseInt(request.params.id);
  const item = trendingItems.find((item) => item.id === id);
  if (item) {
    response.json(item);
  } else {
    response.status(404).end();
  }
});

//? _________DELETE_________________

app.delete("/trendingItems/:id", (request, response) => {
  const id = parseInt(request.params.id);
  trendingItems = trendingItems.filter((item) => item.id !== id);
  response.status(204).end();
});

//? __________POST__________________
app.post("/trendingItems", (request, response) => {
  const item = request.body;

  const ids = trendingItems.map((item) => item.id);
  const maxId = Math.max(...ids);

  const newItem = {
    id: maxId + 1,
    name: item.name,
    description: item.description,
    img: item.img,
    stock: item.stock,
  };

  trendingItems = [...trendingItems, newItem];
  response.status(201).json(newItem);
});

// Debe ser la última, se ejecutan en orden.. y esta es la que toma todas las rutas que no incluimos antes
app.use((request, response) => {
  console.log(request.path);
  response.status(404).json({
    error: "Not found"
  });
});

/* const PORT = 3004; //! En Heroku no podés especificar el puerto, eso te lo da el servidor. Para heroku se usa: */
const PORT = process.env.PORT || 3004;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});



